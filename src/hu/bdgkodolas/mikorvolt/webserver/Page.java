package hu.bdgkodolas.mikorvolt.webserver;

import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;

public interface Page {

	boolean matches(String uri);

	Response serve(IHTTPSession request, Session session) throws Exception;

}
