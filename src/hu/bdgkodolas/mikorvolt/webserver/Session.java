package hu.bdgkodolas.mikorvolt.webserver;

import hu.bdgkodolas.mikorvolt.data.User;
import hu.bdgkodolas.mikorvolt.webserver.game.Game;
import hu.bdgkodolas.mikorvolt.webserver.page.PlayPage;

public class Session {

	public final long id;
	public final long expire;

	public User user;

	public Game currentGame;

	public Session(long id, long expire) {
		this.id = id;
		this.expire = expire;
	}

}
