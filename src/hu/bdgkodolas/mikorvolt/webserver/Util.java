package hu.bdgkodolas.mikorvolt.webserver;


import java.io.PrintWriter;
import java.io.StringWriter;

public class Util {

	public static String exceptionStackTrace(Exception e) {
		// Stack trace to String
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter);
		e.printStackTrace(printWriter);
		printWriter.flush();

		String out = stringWriter.toString();
		printWriter.close();
		return out;
	}

	public static String validateText(String input) {
		if(input.matches("[a-zA-Z0-9éáűőúöüóíÉÁŰŐÚÖÜÓÍ\\?\\.\\-!\\(\\), ]*"))
			return input;
		throw new IllegalArgumentException("Validation error");
	}

	public static String[] validateText(String... inputs) {
		for(String input : inputs)
			validateText(input);
		return inputs;
	}

}
