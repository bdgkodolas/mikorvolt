package hu.bdgkodolas.mikorvolt.webserver;

import fi.iki.elonen.NanoHTTPD;
import hu.bdgkodolas.mikorvolt.data.Backup;
import hu.bdgkodolas.mikorvolt.data.TopicDatabase;
import hu.bdgkodolas.mikorvolt.webserver.page.*;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

public class WebServer extends NanoHTTPD {

	public static final String FORM_LOGOUT = "logout";

	public static final DateFormat[] HTTP_DATE_FORMATS = new DateFormat[] {

			new SimpleDateFormat("EEE, dd MMM yyyy HH:mm:ss z", Locale.US),
			new SimpleDateFormat("EEEE, dd-MMM-yy HH:mm:ss z", Locale.US),
			new SimpleDateFormat("EEE MMM  d HH:mm:ss yyyy", Locale.US)
	};

	static {
		for (DateFormat df : HTTP_DATE_FORMATS)
			df.setTimeZone(TimeZone.getTimeZone("GMT"));
	}

	public static final Response.IStatus NOT_CHANGED = new Response.IStatus() {
		@Override
		public String getDescription() {
			return getRequestStatus() + " Not changed";
		}

		@Override
		public int getRequestStatus() {
			return 304;
		}
	};

	private static final int PORT            = 18100;
	private static final int BACKUP_INTERVAL = 2 * 60 * 60 * 1000;

	private static final String COOKIE_NAME = "cake";

	private static final Page[] PAGES = {
			AboutPage.INSTANCE,
			MainPage.INSTANCE,
			PlayPage.INSTANCE,
			StaticFilePage.INSTANCE,
			TopicEditorPage.INSTANCE,
			TopicPage.INSTANCE
	};

	private static final BackupThread BACKUP_THREAD = new BackupThread(BACKUP_INTERVAL);

	private static final Random RANDOM = new Random();

	private static final String DEFAULT_BACKUP_FILE_NAME = "backup.mvb";

	private static final Map<Long, Session> sessions = new HashMap<>();

	private static PrintStream log;
	private static WebServer   server;
	private static String      backupFileName;

	private static Date launchDate;


	private WebServer() throws IOException {
		super(PORT);
	}


	public static void main(String[] args) {

		Calendar c = Calendar.getInstance();
		c.set(Calendar.MILLISECOND, 0);
		launchDate = c.getTime();

		initLog();
		Scanner sc = new Scanner(System.in);

		restore(sc);
		BACKUP_THREAD.setBackupFile(backupFileName);


		try {
			server = new WebServer();
			startHttps(server);
			server.start(NanoHTTPD.SOCKET_READ_TIMEOUT, false);
			System.out.println("\nRunning! Point your browers to http://localhost:" + PORT + "/ \n");
		} catch (IOException ioe) {
			System.err.println("Couldn't start server:\n" + ioe);
			return;
		}

		BACKUP_THREAD.start();
		waitForQuit(sc);
		BACKUP_THREAD.finish();
		Backup.backup(backupFileName);
	}

	public static Date getLaunchDate() {
		return launchDate;
	}

	public static long parseHttpDate(String httpDate) throws IllegalArgumentException {
		Date date = null;
		for (DateFormat df : HTTP_DATE_FORMATS) {
			try {
				date = df.parse(httpDate);
				break;
			} catch (ParseException e) {
			}
		}
		if (date == null)
			throw new IllegalArgumentException("The parameter is not Http-Date formatted");
		return date.getTime();
	}


	private static void initLog() {
		try {
			log = new PrintStream(new FileOutputStream("log.txt", true), true, "UTF-8");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void restore(Scanner sc) {
		System.out.println("Select the backup file to restore (empty means " + DEFAULT_BACKUP_FILE_NAME + ")");
		String line = sc.nextLine();
		if (line.equals(""))
			line = DEFAULT_BACKUP_FILE_NAME;

		backupFileName = line;

		log("Restore " + backupFileName);
		Backup.restore(backupFileName);
	}

	private static void startHttps(WebServer server) {
		if (System.console() == null) {
			System.out.println("Console not supported, cannot start https");
			return;
		}
		char[] password = System.console().readPassword("Enter the password for keystore.jks (or leave empty)");
		if (password != null && password.length > 0)
			try {
				server.makeSecure(NanoHTTPD.makeSSLSocketFactory("/keystore.jks", password), new String[] {"TLSv1"});
			} catch (IOException e) {
				e.printStackTrace();
			}
	}

	private static void waitForQuit(Scanner sc) {
		System.out.println("Type quit to exit");
		while (true) {
			String oneLine = sc.nextLine();
			if (oneLine.equals("quit")) {
				System.out.println("I am quit.");
				server.stop();
				break;
			}
		}
	}

	private synchronized static void log(Exception e) {
		log(Util.exceptionStackTrace(e));
	}

	private synchronized static void log(IHTTPSession session) {
		log("Lekertek ezt az oldalt :" + session.getUri());
	}

	static synchronized void log(String message) {
		log.println(DateTimeFormatter.ISO_LOCAL_DATE_TIME.format(LocalDateTime.now())
					+ " " + message);
	}

	public static String decodePercent(String string) {
		return NanoHTTPD.decodePercent(string);
	}

	@Override
	public Response serve(IHTTPSession request) {
		super.serve(request);

		log(request);

		Session session = getSession(request);
		logOutIfNecessary(request, session);
		quitGameIfNecessary(session);
		String uri = request.getUri();

		try {
			for (Page page : PAGES) {
				synchronized (TopicDatabase.class) {
					if (page.matches(uri))
						return page.serve(request, session);
				}
			}

			return NotFoundPage.INSTANCE.serve(request, session);
		} catch (Exception e) {
			log(e);
			return new ExceptionPage(e).serve(request, session);
		}
	}

	private static Session getSession(IHTTPSession session) {
		CookieHandler cookieHandler = session.getCookies();
		if (cookieHandler.read(COOKIE_NAME) != null) {
			long id = Long.parseLong(cookieHandler.read(COOKIE_NAME));
			Session out = sessions.get(id);
			if (out != null)
				return out;
		}

		long n = newId();
		Session szeson = new Session(n, 31);
		sessions.put(n, szeson);
		String ID = "" + n;
		cookieHandler.set(COOKIE_NAME, ID, 31);
		return szeson;

	}

	private static void quitGameIfNecessary(Session session) {
		if (session.currentGame != null && session.currentGame.isFinished())
			session.currentGame = null;
	}

	private static void logOutIfNecessary(IHTTPSession request, Session session) {
		if (request.getParms().get(FORM_LOGOUT) != null)
			session.user = null;
	}

	private static long newId() {
		long id;
		do {
			id = RANDOM.nextLong();
		} while (sessions.containsKey(id));
		return id;
	}

}
