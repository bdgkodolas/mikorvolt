package hu.bdgkodolas.mikorvolt.webserver.game;

import hu.bdgkodolas.mikorvolt.data.HistoricalDate;
import hu.bdgkodolas.mikorvolt.data.Question;
import hu.bdgkodolas.mikorvolt.data.Topic;

import java.util.Map;
import java.util.Random;
import java.util.ArrayList;

public class SameYearGame extends Game {

	public static final String NAME = "Ugyanabban az évben történt?";

	private Question[] questions;
	private int     progress = -2; // The index of the current Question
	private boolean answered = true; // Was the current Question answered?
	private int     points   = 0;

	public SameYearGame(Topic topic) {
		super(topic);
		Question[] temporaryQuestions = topic.getQuestions();
		// Creating of a new array which does not contain questions with a century as their answers.
		ArrayList<Question> listOfAppropriateQuestions = new ArrayList<>();
		for (Question question : temporaryQuestions) {
			if (question.getAnswer().getMode() != HistoricalDate.MODE_CENTURY) {
				listOfAppropriateQuestions.add(question);
			}
		}

		if (listOfAppropriateQuestions.size() == 0) {
			//We should not offer this game if the topic is inappropriate. (This is needed to be checked in advance.)
			System.out.println("Please, choose another topic!");
		} else {
			if ((listOfAppropriateQuestions.size() % 2) == 0) {
				//It is possible to make pairs without leaving out a question.
				questions = new Question[listOfAppropriateQuestions.size()];
				for (int i = 0; i < questions.length; i++) {
					questions[i] = new Question(listOfAppropriateQuestions.get(i).getName(),
												listOfAppropriateQuestions.get(i).getAnswer());
				}

				// Shuffle of questions
				for (int i = 0; i < questions.length; i++) {
					Random random = new Random();
					int x = i + random.nextInt(questions.length - i);
					Question temporaryQuestion = new Question(questions[i].getName(), questions[i].getAnswer());
					questions[i] = new Question(questions[x].getName(), questions[x].getAnswer());
					questions[x] = new Question(temporaryQuestion.getName(), temporaryQuestion.getAnswer());
				}

			} else {
				//It is needed to leave out a question.
				questions = new Question[listOfAppropriateQuestions.size() - 1];
				for (int i = 0; i < (questions.length); i++) {
					questions[i] = new Question(listOfAppropriateQuestions.get(i).getName(),
												listOfAppropriateQuestions.get(i).getAnswer());
				}

				// Shuffle of questions
				for (int i = 0; i < questions.length; i++) {
					Random random = new Random();
					int x = i + random.nextInt(questions.length - i);
					Question temporaryQuestion = new Question(questions[i].getName(), questions[i].getAnswer());
					questions[i] = new Question(questions[x].getName(), questions[x].getAnswer());
					questions[x] = new Question(temporaryQuestion.getName(), temporaryQuestion.getAnswer());
				}
			}

		}

	}

	@Override
	public boolean isFinished() {
		return false;
	}

	@Override
	public String serve(Map<String, String> params) {
		if (progress >= questions.length)
			return "Valami hiba történt"; // TODO what kind of errors?

		if (answered) { // The question is answered, let's pick a new one.
			progress = progress + 2;
			answered = false;

			if (progress == questions.length) { // Show the results
				return "<body><p>Ennyi pontot értél el: " + points + "/" + (questions.length / 2)
					   + "</p>";

			} else { // Question
				return "<p>" + ((progress + 2) / 2) + "/" + (questions.length / 2) + "<br>"
					   + " Ugyanabban az évben történtek-e az alábbiak?" + "<br>"
					   + "1, " + questions[progress].getName() + "<br>"
					   + "2, " + questions[progress + 1].getName() + "</p>"
					   + "<form action=\".\" method=\"post\">"
					   + "<button type=\"submit\" name=\"ans\" value=\"yes\">Igen!</button>"
					   + "<button type=\"submit\" name=\"ans\" value=\"no\">Nem!</button>"
					   + "</form>";

			}
		} else { // The answer is arriving now
			answered = true;
			boolean answer = params.get("ans").equals("yes");
			boolean correctAnswer = (questions[progress].getAnswer().getYear() == questions[progress + 1].getAnswer()
					.getYear());

			if (answer == correctAnswer) {
				points++;
				return "<body><p>Helyes</p><form action=\".\" method=\"post\">"
					   + "<button type=\"submit\">Következő</button></form>";
			} else {
				if (correctAnswer) {
					return "<p>Rossz válasz</p><form action=\".\">"
						   + "<p>" + "Ebben az évben történt mindkettő: " + questions[progress].getAnswer().getYear()
						   + "</p>"
						   + "<button type=\"submit\">Következő</button></form>";
				} else {
					return "<p>Rossz válasz</p><form action=\".\">"
						   + "<p>" + questions[progress].getName() + ": " + questions[progress].getAnswer().getYear()
						   + "<br>"
						   + questions[progress + 1].getName() + ": " + questions[progress + 1].getAnswer().getYear()
						   + "</p>"
						   + "<button type=\"submit\">Következő</button></form>";
				}
			}

		}
	}

	@Override
	public String getName() {
		return "Ugyanabban az évben történt? 2";
	}
}
