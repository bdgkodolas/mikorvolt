package hu.bdgkodolas.mikorvolt.webserver.game;

import hu.bdgkodolas.mikorvolt.data.HistoricalDate;
import hu.bdgkodolas.mikorvolt.data.Question;
import hu.bdgkodolas.mikorvolt.data.Topic;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;

public class LearningGame extends Game {

	public static final  String NAME               = "Tanulás…";
	private static final Random random             = new Random();
	/**
	 * BAD + DIFFERENCE = GOOD, where BAD is the number of bad answers for each question
	 */
	private static final int    DIFFERENCE         = 2;
	/**
	 * The maximum size of {@link #questionsNotCompleted}
	 */
	private static final int    QUESTIONS_PARALLEL = 3;

	private static final String KEY_RESULT_PAGE_NAME = "%RESULTNAME%";
	private static final String KEY_RESULT_PAGE_DATE = "%RESULTDATE%";
	private static final String KEY_RESULT_PAGE_GOOD = "%RESULTGOOD%";
	private static final String KEY_RESULT_PAGE_BAD  = "%RESULTBAD%";

	private static final String KEY_QUESTION_NAME  = "%QUESTIONNAME%";
	private static final String KEY_QUESTION_INPUT = "%QUESTIONINPUT%";

	private static final String KEY_RATE_NAME = "%RATENAME%";
	private static final String KEY_RATE_DATE = "%RATEDATE%";

	private static final String KEY_INTRODUCE_NAME = "%INTRODUCENAME%";
	private static final String KEY_INRODUCE_DATE  = "%INTRODUCEDATE%";

	private static final String FORM_QUESTION_YEAR    = "year";
	private static final String FORM_QUESTION_MONTH   = "month";
	private static final String FORM_QUESTION_DAY     = "day";
	private static final String FORM_QUESTION_CENTURY = "century";

	private static final String HTML_RESULT_PAGE_HEADER  =
			"<table border=\"0\">\n"
			+ "    <tr>\n"
			+ "        <th>Esemény</th>\n"
			+ "        <th>Dátum</th>\n"
			+ "        <th>Jó válasz</th>\n"
			+ "        <th>Rossz válasz</th>\n"
			+ "    </tr>\n";
	private static final String HTML_RESULT_PAGE_ONE_ROW =
			"    <tr>\n"
			+ "        <td>" + KEY_RESULT_PAGE_NAME + "</th>\n"
			+ "        <th>" + KEY_RESULT_PAGE_DATE + "</th>\n"
			+ "        <th>" + KEY_RESULT_PAGE_GOOD + "</th>\n"
			+ "        <th>" + KEY_RESULT_PAGE_BAD + "</th>\n"
			+ "    </tr>\n";
	private static final String HTML_RESULT_PAGE_FOOTER  =
			"</table>";


	private static final String HTML_QUESTION                =
			"<form action=\".\" method=\"post\">"
			+ "\t<p>Mikor volt a(z) " + KEY_QUESTION_NAME + "?</p>"
			+ KEY_QUESTION_INPUT
			+ "\t<input type=\"submit\" value=\"Küldés\" />\n"
			+ "</form>";
	private static final String HTML_INTRODUCE               =
			"<p>A(z) " + KEY_INTRODUCE_NAME + " ekkor történt: " + KEY_INRODUCE_DATE + "</p>"
			+ "<form action=\".\" method=\"post\"><input type=\"submit\" value=\"Megjegyeztem!\"></form>";
	private static final String HTML_QUESTION_YEAR           =
			"\t<input type=\"number\" name=\"year\" style=\"width:60px;\" />.\n";
	private static final String HTML_QUESTION_YEAR_MONTH     =
			HTML_QUESTION_YEAR + "\t<input type=\"number\" name=\"month\" style=\"width:30px;\"/>.\n";
	private static final String HTML_QUESTION_YEAR_MONTH_DAY =
			HTML_QUESTION_YEAR_MONTH + "\t<input type=\"number\" name=\"day\" style=\"width:30px;\">.\n";
	private static final String HTML_QUESTION_CENTURY        =
			"\t<input type=\"number\" name=\"century\" style=\"width:50px;\">\n";


	private static final String HTML_RATE_CORRECT =
			"<p>Helyes!</p>\n"
			+ "<form action=\".\"><button "
			+ "type=\"submit\">Következő</button></form>";
	private static final String HTML_RATE_MISS    =
			"<p>Rossz válasz.</p>\n"
			+ "A(z) " + KEY_RATE_NAME + " ekkor történt: " + KEY_RATE_DATE
			+ "<form action=\".\"><button "
			+ "type=\"submit\">Következő</button></form>";

	private static final String HTML_ILLEGAL_FIELD =
			"<p>Hibás mező!</p>";

	private final int totalSize;
	private final ArrayList<QuestionWithGameInfo> questionsNotAsked     = new ArrayList<>();
	private final ArrayList<QuestionWithGameInfo> questionsNotCompleted = new ArrayList<>();
	private final ArrayList<QuestionWithGameInfo> questionsCompleted    = new ArrayList<>();
	private       boolean                         answered              = true;
	private       QuestionWithGameInfo            questionToAsk         = null;
	private       boolean                         finished              = false;

	public LearningGame(Topic topic) {
		super(topic);
		for (Question question : topic.getQuestions())
			questionsNotAsked.add(new QuestionWithGameInfo(question));
		totalSize = questionsNotAsked.size();
	}

	@Override
	public boolean isFinished() {
		return finished;
	}

	@Override
	public String serve(Map<String, String> params) {
		String out;
		if (answered) {
			if (questionsCompleted.size() >= totalSize)
				out = resultPage();
			else
				out = newQuestion();
		} else {
			out = rateAnswer(params);
		}
		return out;
	}

	private String resultPage() {
		finished = true;
		String out = HTML_RESULT_PAGE_HEADER;
		for (QuestionWithGameInfo questionWithGameInfo : questionsCompleted)
			out += HTML_RESULT_PAGE_ONE_ROW
					.replace(KEY_RESULT_PAGE_NAME, questionWithGameInfo.question.getName())
					.replace(KEY_RESULT_PAGE_DATE, questionWithGameInfo.question.getAnswer().toString())
					.replace(KEY_RESULT_PAGE_GOOD, questionWithGameInfo.good + "")
					.replace(KEY_RESULT_PAGE_BAD, questionWithGameInfo.bad + "");
		out += HTML_RESULT_PAGE_FOOTER;
		return out;
	}

	private String newQuestion() {
		if (questionsNotCompleted.size() < QUESTIONS_PARALLEL && questionsNotAsked.size() > 0)
			return introduceNewQuestion();

		answered = false;
		questionToAsk = questionsNotCompleted.get(random.nextInt(questionsNotCompleted.size()));

		return constructNewQuestionPage();
	}

	private String introduceNewQuestion() {
		answered = true;

		questionToAsk = questionsNotAsked.remove(random.nextInt(questionsNotAsked.size()));
		questionsNotCompleted.add(questionToAsk);

		return HTML_INTRODUCE
				.replace(KEY_INTRODUCE_NAME, questionToAsk.question.getName())
				.replace(KEY_INRODUCE_DATE, questionToAsk.question.getAnswer().toString());
	}

	private String constructNewQuestionPage() {
		Question question = questionToAsk.question;
		String out = HTML_QUESTION
				.replace(KEY_QUESTION_NAME, question.getName());

		String inputFields;
		switch (question.getAnswer().getMode()) {
			case HistoricalDate.MODE_YEAR:
				inputFields = HTML_QUESTION_YEAR;
				break;
			case HistoricalDate.MODE_YEAR_MONTH:
				inputFields = HTML_QUESTION_YEAR_MONTH;
				break;
			case HistoricalDate.MODE_YEAR_MONTH_DAY:
				inputFields = HTML_QUESTION_YEAR_MONTH_DAY;
				break;
			case HistoricalDate.MODE_CENTURY:
				inputFields = HTML_QUESTION_CENTURY;
				break;
			default:
				System.err.println("Bad implementation");
				System.err.println(Arrays.toString(Thread.currentThread().getStackTrace()));
				return null;
		}
		out = out.replace(KEY_QUESTION_INPUT, inputFields);
		return out;
	}

	private String rateAnswer(Map<String, String> parms) {
		try {
			if (checkAnswer(parms)) {
				answered = true;
				++questionToAsk.good;
				if (questionToAsk.good - questionToAsk.bad >= DIFFERENCE) {
					questionCompleted();
				}
				return HTML_RATE_CORRECT;
			} else {
				answered = true;
				++questionToAsk.bad;
				return HTML_RATE_MISS.replace(KEY_RATE_NAME, questionToAsk.question.getName())
						.replace(KEY_RATE_DATE, questionToAsk.question.getAnswer().toString());
			}
		} catch (NumberFormatException e) { // Illegal field
			// Resend question page
			return HTML_ILLEGAL_FIELD + constructNewQuestionPage();
		}
	}

	private boolean checkAnswer(Map<String, String> parms) {
		HistoricalDate date = questionToAsk.question.getAnswer();
		switch (date.getMode()) {
			case HistoricalDate.MODE_YEAR_MONTH_DAY:
				if (date.getDay() != Integer.parseInt(parms.get(FORM_QUESTION_DAY)))
					return false;
			case HistoricalDate.MODE_YEAR_MONTH:
				if (date.getMonth() != Integer.parseInt(parms.get(FORM_QUESTION_MONTH)))
					return false;
			case HistoricalDate.MODE_YEAR:
				return date.getYear() == Integer.parseInt(parms.get(FORM_QUESTION_YEAR));
			case HistoricalDate.MODE_CENTURY:
				return date.getCentury() == Integer.parseInt(FORM_QUESTION_CENTURY);
			default:
				System.err.println("Bad implementation");
				System.err.println(Arrays.toString(Thread.currentThread().getStackTrace()));
				return false;
		}
	}

	private void questionCompleted() {
		questionsNotCompleted.remove(questionToAsk);
		questionsCompleted.add(questionToAsk);
	}


	@Override
	public String getName() {
		return NAME;
	}

	private class QuestionWithGameInfo {
		public Question question;
		/**
		 * Number of good answers for this question
		 */
		int good = 0;
		int bad  = 0;

		QuestionWithGameInfo(Question question) {
			this.question = question;
		}
	}
}
