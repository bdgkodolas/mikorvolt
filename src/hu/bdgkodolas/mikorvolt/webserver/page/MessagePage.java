package hu.bdgkodolas.mikorvolt.webserver.page;


import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;

public class MessagePage implements Page {

	private static final String KEY_TITLE   = "%TITLE%";
	private static final String KEY_MESSAGE = "%MESSAGE%";

	private static final String HEADER   =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n"
			+ "<html>\n"
			+ "<head>\n"
			+ "    <title>" + KEY_TITLE + "</title>\n"
			+ "    <meta http-equiv=\"content-type\" content=\"text/html;charset=utf-8\" />\n"
			+ "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
			+ "    <meta name=\"description\" content=\"\">\n"
			+ "    <meta name=\"author\" content=\"\">\n"
			+ "    <link href=\"/css/style.css\" rel=\"stylesheet\">\n"
			+ "    <link href=\"/files/favicon.png\" rel=\"icon\">\n"
			+ "    <style>\n"
			+ "        .bigbutton{\n"
			+ "            text-decoration: none;\n"
			+ "            font-family: Agency FB;\n"
			+ "            color: white;\n"
			+ "            border-radius: 12px;\n"
			+ "            height: 36px;\n"
			+ "            width: 300px;\n"
			+ "            background-color: rgb(222, 80, 37);\n"
			+ "            border: 5px solid;\n"
			+ "            text-align: center;\n"
			+ "            margin-left: auto;\n"
			+ "            margin-right: auto;\n"
			+ "            height: 40px;\n"
			+ "        }\n"
			+ "        #gameContainer{\n"
			+ "            background-color: white;\n"
			+ "            border-radius: 24px;\n"
			+ "            height: 60%;\n"
			+ "            width: 80%;\n"
			+ "            border: 5px solid;\n"
			+ "            border-color: white;\n"
			+ "            padding: 12px;\n"
			+ "        }\n"
			+ "    </style>\n"
			+ "</head>\n"
			+ "<body style=\"background-image: url(/files/history-wallpaper-9.jpg);background-position:center top;"
			+ "\">\n"
			+ "<br><br>\n"
			+ "<table style=\"text-align: left; width: 100%; height: 64px;\" border=\"0\" cellpadding=\"0\" "
			+ "cellspacing=\"5\">\n"
			+ "    <tbody>\n"
			+ "    <tr>\n"
			+ "        <td>\n"
			+ "            <big style=\"color: rgb(222, 80, 37);\">\n"
			+ "                <big><big><big><big>\n"
			+ "\t\t\t\t\t\t\t\t<span style=\"font-family: Agency FB;\">&nbsp;&nbsp;\n"
			+ "\t\t\t\t\t\t\t\t    <span style=\"font-weight: bold;\">" + KEY_TITLE + "</span>\n"
			+ "\t\t\t\t\t\t\t\t</span>\n"
			+ "                </big></big></big></big>\n"
			+ "            </big>\n"
			+ "        </td>\n"
			+ "    </tr>\n"
			+ "    </tbody>\n"
			+ "</table>\n"
			+ "<br><br><br>\n"
			+ "<center>\n"
			+ "    <div id=\"gameContainer\">\n"
			+ "        <p>\n";
	private static final String ONE_LINE =
			"\t\t\t\t" + KEY_MESSAGE + "<br>\n";
	private static final String FOOTER   =
			"        </p>\n"
			+ "    </div>\n"
			+ "</center>\n"
			+ "</body>\n"
			+ "</html>\n";
	private String message;
	private String title;

	protected MessagePage() {
	}

	public MessagePage(String title, String message) {
		setMessage(message);
		setTitle(title);
	}

	public String getTitle() {
		return title;
	}

	protected void setTitle(String title) {
		this.title = title;
	}

	public String getMessage() {
		return message;
	}

	protected void setMessage(String message) {
		this.message = message;
	}

	@Override
	public boolean matches(String uri) {
		return false;
	}

	@Override
	public Response serve(IHTTPSession request, Session session) {
		String out = HEADER;
		String[] lines = message.split("\n");
		for (String line : lines)
			out += ONE_LINE.replace(KEY_MESSAGE, line);
		out += FOOTER;

		out = out.replace(KEY_TITLE, title);

		return NanoHTTPD.newFixedLengthResponse(out);
	}
}
