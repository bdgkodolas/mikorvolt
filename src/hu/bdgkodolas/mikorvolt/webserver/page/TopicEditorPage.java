package hu.bdgkodolas.mikorvolt.webserver.page;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.data.*;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;
import hu.bdgkodolas.mikorvolt.webserver.Util;
import hu.bdgkodolas.mikorvolt.webserver.WebServer;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TopicEditorPage implements Page {

	public static final  Page   INSTANCE = new TopicEditorPage();
	private static final String URI      = "/editTopic/";

	private static final String FORM_TOPIC_ID   = "topic";
	private static final String FORM_MODE       = "mode";
	private static final String FORM_TOPIC_NAME = "topicName";
	private static final String MODE_EDIT       = "edit";
	private static final String MODE_DELETE     = "delete";
	private static final String MODE_ADD        = "add";

	private static final Page SUCCESS_PAGE = new MessagePage("Téma módosítás",
															 "Siker :)<br><a href=\"" + URI + "\">Vissza</a>");

	private static final String KEY_TOPIC_ID            = "%TOPIC_ID%";
	private static final String KEY_USERNAME            = "%USERNAME%";
	private static final String KEY_TOPIC_NAME          = "%TOPIC_NAME%";
	private static final String KEY_TOPIC_DESCRIPTION   = "%TOPIC_DESCRIPTION%";
	private static final String KEY_COMMA_SEPARATED_KWS = "%TOPIC_KWS%";

	private static final String KEY_ROW_QUESTION_NAME   = "%QUESTION_NAME%";
	private static final String KEY_ROW_QUESTION_ANSWER = "%QUESTION_ANSWER%";

	private static final String KEY_SELECTOR_ID          = "%ID%";
	private static final String KEY_SELECTOR_NAME        = "%SELECTOR_TOPIC_NAME%";
	private static final String KEY_SELECTOR_DESCRIPTION = "%SELECTOR_TOPIC_DESCRIPTION%";

	private static final String HTML_ONE_ROW_TR_INNER =
			"<td><input type=\"text\" name=\"questionNames\" value=\"" + KEY_ROW_QUESTION_NAME + "\">\n"
			+ "<td><input type=\"text\" name=\"questionAnswers\" value=\"" + KEY_ROW_QUESTION_ANSWER + "\"></td>\n"
			+ "<td><button onclick=\"deleteRow(this)\">Törlés</button></td>\n";
	private static final String HTML_ONE_ROW          =
			"<tr>\n"
			+ HTML_ONE_ROW_TR_INNER
			+ "</tr>\n";
	private static final String HTML_EDIT_HEADER      =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n"
			+ "<html>\n"
			+ "\t<head>\n"
			+ "\t\t<meta content=\"text/html; charset=utf-8\" http-equiv=\"content-type\">\n"
			+ "\t\t<title>Mikorvolt - Saját1</title>\n"
			+ "\t\t<meta charset=\"utf-8\">\n"
			+ "\t\t<style type=\"text/css\">\n"
			+ "            body {\n"
			+ "            border-style: hidden;\n"
			+ "            border-width: 64px;\n"
			+ "            margin: 64px;\n"
			+ "            background-image: url(\"../files/history-wallpaper-9.jpg\");\n"
			+ "            background-attachment: fixed;\n"
			+ "            background-position: center top;\n"
			+ "            }\n"
			+ "            #qCContainer{\n"
			+ "\t\t\t\tbackground-color: white;\n"
			+ "\t\t\t\tborder-radius: 24px;\n"
			+ "\t\t\t\twidth: 80%\n"
			+ "\t\t\t\tborder-color: white;\n"
			+ "\t\t\t}\n"
			+ "            #questionContainer {\n"
			+ "\t\t\t\tbackground-color: white;\n"
			+ "\t\t\t\tborder-radius: 24px;\n"
			+ "            }\n"
			+ "            #questionContainer tr td:nth-child(3) {\n"
			+ "\t\t\t\ttext-align: right;\n"
			+ "\t\t\t}\n"
			+ "\t\t</style>\n"
			+ "<script>\n"
			+ "function deleteRow(button) {\n"
			+ "\tvar rowIndex = button.parentNode.parentNode.rowIndex;\n"
			+ "\tdocument.getElementById('questionContainer').deleteRow(rowIndex);\n"
			+ "}\n"
			+ "function addRow() {\n"
			+ "\tdocument.getElementById('questionContainer').insertRow(-1).innerHTML += '"
			+ HTML_ONE_ROW_TR_INNER
					.replace("\n", "\\\n")
					.replace(KEY_ROW_QUESTION_NAME, "")
					.replace(KEY_ROW_QUESTION_ANSWER, "")
			+ "';"
			+ "}\n"
			+ "</script>\n"
			+ "\t</head>\n"
			+ "\t<body style=\"background-image: url(../files/history-wallpaper-9.jpg);\">\n"
			+ "\t\t<form action=\".\" method=\"post\">\n"
			+ "\t\t\t<input type=\"hidden\" name=\"topic\" value=\"" + KEY_TOPIC_ID + "\">"
			+ "\t\t\t<fieldset>\n"
			+ "\t\t\t\t<table style=\"text-align: left; width: 100%; height: 64px;\" border=\"0\" cellpadding=\"0\" "
			+ "cellspacing=\"5\">\n"
			+ "\t\t\t\t\t<tbody>\n"
			+ "\t\t\t\t\t\t<tr>\n"
			+ "\t\t\t\t\t<td style=\"width:48px;\">\n"
			+ "\t\t\t\t\t\t<a href=\".\">\n"
			+ "\t\t\t\t\t\t\t<img style=\"border: 0px solid ; width: 48px; height: 48px;\" alt=\"\" "
			+ "title=\"Visszalépés\" src=\"../files/back.png\">\n"
			+ "\t\t\t\t\t\t</a>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t\t\t<input type=\"text\" style=\"color: rgb(222, 80, 37); font-family: Agency FB;"
			+ "margin-left:50px\" "
			+ "name=\"topicName\" value=\"" + KEY_TOPIC_NAME + "\">\n"
			+ "\t\t\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t\t\t<td style=\"text-align: right;\">\n"
			+ "\t\t\t\t\t\t\t<input type=\"submit\" value=\"Küldés\">"
			+ "\t\t\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t\t</tr>\n"
			+ "\t\t\t\t\t</tbody>\n"
			+ "\t\t\t\t</table>\n"
			+ "\t\t\t\t<br>\n"
			+ "\t\t\t\t<input type=\"text\" style=\"color: black; font-family: Arial; margin-left: 15%; display:block;"
			+ "\" "
			+ "name=\"topicDescription\" value=\"" + KEY_TOPIC_DESCRIPTION + "\">\n"
			+ "\t\t\t\t<input type=\"text\" style=\"color: black; font-family: Arial; margin-left: 15%; display:block;"
			+ "\" "
			+ "name=\"topicKeywords\" value=\"" + KEY_COMMA_SEPARATED_KWS + "\">\n"
			+ "\t\t\t</fieldset>\n"
			+ "\t\t\t<input type=\"hidden\" name=\"topic\" value=\"" + KEY_TOPIC_ID + "\">"
			+ "\t\t\t<fieldset>\n"
			+ "\t\t\t\t<center>\n"
			+ "\t\t\t\t\t<div id=\"qCContainer\">\n"
			+ "\t\t\t\t\t\t<table id=\"questionContainer\" style=\"margin:20px auto;\" border=\"0\" cellpadding=\"0\" "
			+ "cellspacing=\"2\">\n"
			+ "\t\t\t\t\t\t\t<tbody>";

	private static final String HTML_EDIT_FOOTER =
			"\n"
			+ "\t\t\t\t\t\t\t</tbody>\n"
			+ "\t\t\t\t\t\t</table>\n"
			+ "\t\t\t\t\t\t<center>\n"
			+ "\t\t\t\t\t\t\t<button type=\"button\" onclick=\"addRow()\">\n"
			+ "\t\t\t\t\t\t\t\t<img title=\"Kérdés hozzáadása\" style=\"border: 0px solid ; width: 48px; height: 48px;"
			+ "\" alt=\"\" src=\"../files/add.png\">\n"
			+ "\t\t\t\t\t\t\t</button>\n"
			+ "\t\t\t\t\t\t</center>\n"
			+ "\t\t\t\t\t</div>\n"
			+ "\t\t\t\t</center>\n"
			+ "\t\t\t</fieldset>\n"
			+ "\t\t</form>\n"
			+ "\t</body>\n"
			+ "</html>\n";

	private static final String HTML_TOPIC_SELECTOR_HEADER  =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" "
			+ "\"http://www.w3.org/TR/html4/strict.dtd\">\n"
			+ "<html>\n"
			+ "    <head> \n"
			+ "        <meta content=\"text/html; charset=utf-8\" "
			+ "http-equiv=\"content-type\"> \n"
			+ "        <title>Mikorvolt - a Te Mikorvoltod</title>  "
			+ "       \n"
			+ "        <meta charset=\"utf-8\"> \n"
			+ "        <style> \n"
			+ "            .urTopic {\n"
			+ "            color: rgb(222, 80, 37); \n"
			+ "            text-decoration: none;\n"
			+ "            }\n"
			+ "            .bigbutton {\n"
			+ "            text-decoration: none;\n"
			+ "            font-family: Agency FB;\n"
			+ "            color: white;\n"
			+ "            border-radius: 12px;\n"
			+ "            height: 40px;\n"
			+ "            width: 300px;\n"
			+ "            background-color: rgb(222, 80, 37);\n"
			+ "            border: 5px solid;\n"
			+ "            padding-top: 0px;\n"
			+ "            padding-bottom: 20px;\n"
			+ "            }\n"
			+ "\t\t</style>\n"
			+ "\t\t<script>\n"
			+ "\t\t\tfunction addT() {\n"
			+ "\t\t\t\tvar tTable = document.getElementById"
			+ "(\"TopicContainer\");\n"
			+ "\n"
			+ "\t\t\t\tvar newRow = tTable.insertRow(tTable.rows"
			+ ".length);\n"
			+ "\n"
			+ "\t\t\t\tvar newTTxt = newRow.insertCell(0);\n"
			+ "\t\t\t\tvar newTDel = newRow.insertCell(1);\n"
			+ "\t\t\t\tnewTDel.style.textAlign='right';\n"
			+ "\n"
			+ "\t\t\t\tnewTTxt.innerHTML = \"<form action=\\\".\\\" method=\\\"post\\\">"
			+ "<input type=\\\"hidden\\\" name=\\\"" + FORM_MODE + "\\\" value=\\\"" + MODE_ADD + "\\\">"
			+ "<input type=\\\"text\\\" name=\\\"" + FORM_TOPIC_NAME + "\\\" "
			+ "style=\\\"font-family: Arial;\\\" value=\\\"Új témakör címe\\\">"
			+ "<input type=\\\"submit\\\" value=\\\"Hozzáad\\\">"
			+ "</form>\";\n"
			+ "\n"
			+ "\t\t\t\tnewTDel.innerHTML = \"<button "
			+ "type=\\\"button\\\" onclick=\\\"delT(this)\\\" "
			+ "name=\\\"cancelNew\\\"><img title=\\\"Törlés\\\" "
			+ "style=\\\"border: 0px solid ; width: 48px; height: "
			+ "48px;\\\" alt=\\\"\\\" src=\\\"../files/delete"
			+ ".png\\\"></button>\";\n"
			+ "\t\t\t}\n"
			+ "\n"
			+ "\t\t\tfunction delT(buttonObj) {\n"
			+ "\t\t\t\tvar qTable = document.getElementById"
			+ "(\"TopicContainer\");\n"
			+ "\t\t\t\tvar rowNo = buttonObj.parentNode.parentNode.rowIndex;\n"
			+ "\n"
			+ "\t\t\t\tqTable.deleteRow(rowNo);\n"
			+ "\t\t\t}\n"
			+ "\t\tfunction deleteTopic(id) {\n"
			+ "\t\t\tif(confirm(\"Biztosan törlöd?\")) {\n"
			+ "\t\t\t\tdocument.getElementById('mode'+id).value='" + MODE_DELETE + "';\n"
			+ "\t\t\t\tdocument.getElementById('form'+id).submit();\n"
			+ "\t\t\t}\n"
			+ "\t\t}\n"
			+ "\t\t</script>\n"
			+ "    </head>\n"
			+ "    <body>\n"
			+ "        <table "
			+ "background=\"../files/history-wallpaper-9.jpg\" "
			+ "style=\"text-align: left; width: 100%; height: 200px;\""
			+ " border=\"0\" cellpadding=\"0\" cellspacing=\"5\">\n"
			+ "\t\t\t<tbody>\n"
			+ "\t\t\t\t<tr>\n"
			+ "\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t<a href=\"../\">\n"
			+ "\t\t\t\t\t\t\t<img style=\"border: 0px solid ; width: 48px; height: 48px;\" alt=\"\" "
			+ "title=\"Visszalépés\" src=\"../files/back.png\">\n"
			+ "\t\t\t\t\t\t</a>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t<big style=\"color: rgb(222, 80, 37);"
			+ "\">\n"
			+ "\t\t\t\t\t\t\t<big><big><big><big><big>\n"
			+ "\t\t\t\t\t\t\t\t<span style=\"font-family: Agency FB;\">"
			+ "\t\t\t\t\t\t\t\t\t<span style=\"font-weight: bold;"
			+ "\">Témaköreid</span>                 \n"
			+ "\t\t\t\t\t\t\t\t</span>\n"
			+ "\t\t\t\t\t\t\t</big></big></big></big></big><br><br"
			+ ">\n"
			+ "\t\t\t\t\t\t</big>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t<td style=\"text-align: right; "
			+ "vertical-align: center;\">\n"
			+ "\t\t\t\t\t\t<big><big><big> <br>\n"
			+ "\t\t\t\t\t\t\t<span style=\"color: white; "
			+ "font-family: Agency FB; padding-bottom: 10px\">\n"
			+ "\t\t\t\t\t\t\t\t" + KEY_USERNAME + "\n"
			+ "\t\t\t\t\t\t\t</span>\n"
			+ "\t\t\t\t\t\t</big></big></big>\n"
			+ "\t\t\t\t\t\t<form action=\"/\" method=\"post\">\n"
			+ "\t\t\t\t\t\t\t<input type=\"hidden\" name=\"" + WebServer.FORM_LOGOUT + "\">\n"
			+ "\t\t\t\t\t\t\t<input type=\"submit\" value=\"Kijelentkezés\">\n"
			+ "\t\t\t\t\t\t</form>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t</tr>\n"
			+ "\t\t\t</tbody>\n"
			+ "\t\t</table>\n"
			+ "        <br>         \n"
			+ "\t\t<br>\n"
			+ "\t\t<br>\n"
			+ "\t\t<table id=\"TopicContainer\" style=\"text-align: "
			+ "left; width: 80%\" border=\"0\" cellpadding=\"0\" "
			+ "cellspacing=\"2\" align=\"right\">\n"
			+ "\t\t\t<tbody>";
	private static final String HTML_TOPIC_SELECTOR_FOOTER  =
			"\t\t\t</tbody>\n"
			+ "\t\t</table>\n"
			+ "\t\t<center>\n"
			+ "\t\t\t<button type=\"button\" onclick=\"addT()\" name=\"newT\">\n"
			+ "\t\t\t\t<img title=\"Témakör hozzáadása\" style=\"border: 0px solid ; width: 48px; height: 48px;\" "
			+ "alt=\"\" src=\"../files/add.png\">\n"
			+ "\t\t\t</button>\n"
			+ "\t\t</center>\n"
			+ "    </body>\n"
			+ "</html>\n";
	private static final String HTML_TOPIC_SELECTOR_ONE_ROW =
			"<form method=\"post\" action=\".\" id=\"form" + KEY_SELECTOR_ID + "\">\n"
			+ "<input type=\"hidden\" name=\"" + FORM_MODE + "\" id=\"mode" + KEY_SELECTOR_ID + "\">"
			+ "<input type=\"hidden\" name=\"" + FORM_TOPIC_ID + "\" value=\"" + KEY_SELECTOR_ID + "\">"
			+ "<tr>\n"
			+ "\t\t\t\t\t<td>\n"
			+ "\t\t\t\t\t\t<dl> \n"
			+ "\t\t\t\t\t\t\t<dt style=\"font-family: Agency FB;\">\n"
			+ "\t\t\t\t\t\t\t\t<a class=\"urTopic\" href=\"javascript:"
			+ "document.getElementById('mode" + KEY_SELECTOR_ID + "').value='" + MODE_EDIT + "';"
			+ "document.getElementById('form" + KEY_SELECTOR_ID + "').submit();\">\n"
			+ "\t\t\t\t\t\t\t\t\t<big><big><big>\n"
			+ "\t\t\t\t\t\t\t\t\t\t<span>" + KEY_SELECTOR_NAME + "</span>\n"
			+ "\t\t\t\t\t\t\t\t\t</big></big></big>\n"
			+ "\t\t\t\t\t\t\t\t</a>\n"
			+ "\t\t\t\t\t\t\t</dt> \n"
			+ "\t\t\t\t\t\t\t<dd style=\"font-family: Arial;\">" + KEY_SELECTOR_DESCRIPTION + "</dd>\n"
			+ "\t\t\t\t\t\t</dl>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t\t<td style=\"text-align: right;\">\n"
			+ "\t\t\t\t\t\t<button type=\"button\" onclick=\"deleteTopic('" + KEY_SELECTOR_ID + "')\">"
			+ "\t\t\t\t\t\t\t<img title=\"Törlés\" style=\"border: 0px solid ; width: 48px; height: 48px;\" alt=\"\" "
			+ "src=\"../files/delete.png\">\n"
			+ "\t\t\t\t\t\t</button>\n"
			+ "\t\t\t\t\t</td>\n"
			+ "\t\t\t\t</tr>"
			+ "</form>\n";

	private TopicEditorPage() {
	}

	private static String[] processFormArray(String rawParams, String inputName) {
		String[] splits = rawParams.split("&");
		List<String> out = new ArrayList<>();
		for (String split : splits) {
			if (split.startsWith(inputName + "="))
				out.add(Util.validateText(WebServer.decodePercent(split.substring(inputName.length() + 1))));
		}
		return out.toArray(new String[out.size()]);
	}

	private static boolean hasAccess(User user, Topic topic) {
		return topic.getOwner().equals(user);
	}

	@Override
	public boolean matches(String uri) {
		return uri.equals(URI);
	}

	@Override
	public Response serve(IHTTPSession request, Session session) throws IllegalAccessException {
		Map<String, String> parms = request.getParms();
		if (parms.get(FORM_TOPIC_ID) != null) { // A topic is selected
			return topicSelectedPage(request, session);
		} else if (parms.get(FORM_MODE) != null && parms.get(FORM_MODE).equals(MODE_ADD)) {
			return addTopic(request, session);
		} else { // Select a topic
			return topicSelectorPage(session);
		}
	}

	private Response topicSelectedPage(IHTTPSession request, Session session) throws IllegalAccessException {
		Map<String, String> parms = request.getParms();
		long topicId = validateTopicId(parms.get(FORM_TOPIC_ID));
		String mode = parms.get(FORM_MODE);

		// An existing topic is selected
		Topic topic = TopicDatabase.getTopic(topicId);

		if (!hasAccess(session.user, topic))
			throw new IllegalAccessException("Nincs jogod a témakört szerkeszteni.");

		FormData formData = new FormData();
		formData.topicName = parms.get("topicName");
		formData.topicDescription = parms.get("topicDescription"); // TODO FORM_...
		formData.topicKeywords = parms.get("topicKeywords");

		if (formData.topicName != null && formData.topicDescription != null
			&& formData.topicKeywords != null) { // Form was submitted

			Util.validateText(formData.topicName, formData.topicDescription, formData.topicKeywords);

			return formSubmittedPage(request, session, topic, formData);

		} else { // List current questions
			switch (mode) {
				case MODE_EDIT:
					return editorPage(topic);
				case MODE_DELETE:
					return deleteTopic(request, session, topic);
			}
			throw new IllegalArgumentException("Nincs ilyen mód");
		}

	}

	private Response formSubmittedPage(IHTTPSession request, Session session, Topic topic, FormData formData) {

		updateTopicInfo(topic, formData);
		updateTopicQuestions(topic, request.getParms());

		return successPage(request, session);
	}

	private long validateTopicId(String id) throws IllegalArgumentException {
		Long longId = Long.parseLong(id);
		if (TopicPageUtils.checkTopicId(longId))
			return longId;
		else
			throw new IllegalArgumentException("Hibás topicId");
	}

	private void updateTopicInfo(Topic topic, FormData formData) {
		topic.setName(formData.topicName);
		topic.setDescription(formData.topicDescription);
		topic.setKeywords(formData.topicKeywords.split(", *"));
	}

	private void updateTopicQuestions(Topic topic, Map<String, String> parms) {

		String[] questionNames = processFormArray(parms.get("NanoHttpd.QUERY_STRING"), "questionNames");
		String[] questionAnswers = processFormArray(parms.get("NanoHttpd.QUERY_STRING"),
													"questionAnswers");

		topic.clear();
		if (questionNames.length != questionAnswers.length)
			throw new IllegalArgumentException("Hibás bemenet");

		for (int i = 0; i < questionNames.length; ++i)
			topic.addQuestion(new Question(questionNames[i], new HistoricalDate(questionAnswers[i])));
	}

	private Response successPage(IHTTPSession request, Session session) {
		try {
			return SUCCESS_PAGE.serve(request, session);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private Response editorPage(Topic topic) {
		String commaSeperatedKws = "";
		String[] kw = topic.getKeywords();
		if (kw.length > 0) {
			commaSeperatedKws += kw[0];
			for (int i = 1; i < kw.length; ++i)
				commaSeperatedKws += ", " + kw[i];
		}
		String out = HTML_EDIT_HEADER.replace(KEY_TOPIC_NAME, topic.getName())
				.replace(KEY_TOPIC_DESCRIPTION, topic.getDescription())
				.replace(KEY_COMMA_SEPARATED_KWS, commaSeperatedKws)
				.replace(KEY_TOPIC_ID, topic.getId() + "");

		for (Question question : topic.getQuestions()) {
			out += HTML_ONE_ROW.replace(KEY_ROW_QUESTION_NAME, question.getName())
					.replace(KEY_ROW_QUESTION_ANSWER, question.getAnswer().toString());
		}
		out += HTML_EDIT_FOOTER;
		return NanoHTTPD.newFixedLengthResponse(out);

	}

	private Response topicSelectorPage(Session session) {
		String out = HTML_TOPIC_SELECTOR_HEADER;
		for (Topic topic : TopicDatabase.getTopics()) {
			if (topic.getOwner().equals(session.user))
				out += HTML_TOPIC_SELECTOR_ONE_ROW
						.replace(KEY_SELECTOR_ID, topic.getId() + "")
						.replace(KEY_SELECTOR_NAME, topic.getName())
						.replace(KEY_SELECTOR_DESCRIPTION, topic.getDescription());
		}
		out += HTML_TOPIC_SELECTOR_FOOTER;
		out = out.replace(KEY_USERNAME, session.user.getName());
		return NanoHTTPD.newFixedLengthResponse(out);
	}

	private Response deleteTopic(IHTTPSession request, Session session, Topic topic) {
		TopicDatabase.deleteTopic(topic.getId());
		return successPage(request, session);
	}

	private Response addTopic(IHTTPSession request, Session session) {
		if (session.user == null)
			return new ExceptionPage("Nem vagy bejelentkezve").serve(request, session);
		String name = Util.validateText(request.getParms().get(FORM_TOPIC_NAME));
		Topic newTopic = new Topic(name, "", new String[0], session.user);
		try {
			TopicDatabase.addTopic(newTopic);
		} catch (Exception e) {
			return new ExceptionPage(e).serve(request, session);
		}
		return successPage(request, session);
	}

	private class FormData {
		String topicName;
		String topicDescription;
		String topicKeywords;
	}
}
