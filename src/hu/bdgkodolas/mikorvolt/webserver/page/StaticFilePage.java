package hu.bdgkodolas.mikorvolt.webserver.page;


import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;
import hu.bdgkodolas.mikorvolt.webserver.WebServer;

import java.io.File;
import java.io.IOException;
import java.net.URL;

public class StaticFilePage implements Page {

	public static final  Page   INSTANCE = new StaticFilePage();
	private static final String PREFIX   = "/files/";
	private static final String RESOURCE_DIRECTORY;

	static {
		String path = null;
		try {
			path = new File("/hu/bdgkodolas/mikorvolt/webserver/files/")
						   .getCanonicalPath() + "/";
		} catch (IOException e) {
			e.printStackTrace();
		}
		RESOURCE_DIRECTORY = path;
	}

	private static final String HEADER_LAST_MODIFIED     = "Last-Modified";
	private static final String HEADER_IF_MODIFIED_SINCE = "if-modified-since";


	private StaticFilePage() {
	}


	@Override
	public boolean matches(String uri) {
		return uri.startsWith(PREFIX);
	}

	@Override
	public Response serve(IHTTPSession request, Session session) throws IOException {
		String uri = request.getUri();
		int pontpoz = uri.lastIndexOf('.');
		String fileName = RESOURCE_DIRECTORY + uri.substring(PREFIX.length());
		validatePath(fileName);
		String extension = uri.substring(pontpoz + 1);
		String mime = NanoHTTPD.mimeTypes().get(extension);

		URL resourceURL = WebServer.class.getResource(fileName);

		if (notChanged(request)) {
			return NanoHTTPD.newFixedLengthResponse(WebServer.NOT_CHANGED, mime, "");
		} else if (resourceURL != null) { // resource exists

			File file = new File(resourceURL.getFile());
			long size = file.length();
			Response out = new Response(Response.Status.OK, mime,
										WebServer.class.getResourceAsStream(fileName), size > 0 ? size : -1);
			out.addHeader(HEADER_LAST_MODIFIED,
						  WebServer.HTTP_DATE_FORMATS[0].format(WebServer.getLaunchDate()));
			return out;
		} else {
			return NanoHTTPD.newFixedLengthResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_HTML, "Not found");
		}
	}

	private void validatePath(String path) throws IOException {
		String canonicalPath = new File(path).getCanonicalPath(); // TODO it follows symlinks
		if (!canonicalPath.startsWith(RESOURCE_DIRECTORY))
			throw new IllegalArgumentException("Pathname is invalid");
	}

	private boolean notChanged(IHTTPSession request) {
		String ifModifiedSince = request.getHeaders().get(HEADER_IF_MODIFIED_SINCE);
		if (ifModifiedSince == null)
			return false;
		try {
			long time = WebServer.parseHttpDate(ifModifiedSince);
			return time == WebServer.getLaunchDate().getTime();
		} catch (IllegalArgumentException e) {
			return false;
		}
	}
}
