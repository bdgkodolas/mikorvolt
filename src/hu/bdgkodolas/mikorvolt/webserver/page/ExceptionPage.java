package hu.bdgkodolas.mikorvolt.webserver.page;


import hu.bdgkodolas.mikorvolt.webserver.Util;

import java.io.PrintWriter;
import java.io.StringWriter;

public class ExceptionPage extends MessagePage {

	private static final String TITLE = "Hiba";

	public ExceptionPage(Exception exception) {

		setMessage(Util.exceptionStackTrace(exception));

		setTitle(TITLE);
	}

	public ExceptionPage(String message) {
		setMessage(message);
		setTitle(TITLE);
	}
}
