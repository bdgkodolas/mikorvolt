package hu.bdgkodolas.mikorvolt.webserver.page;


import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.data.Topic;
import hu.bdgkodolas.mikorvolt.data.TopicDatabase;
import hu.bdgkodolas.mikorvolt.data.User;
import hu.bdgkodolas.mikorvolt.data.UserDatabase;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;
import hu.bdgkodolas.mikorvolt.webserver.Util;

public class MainPage implements Page {

	public static final  Page   INSTANCE = new MainPage();
	private static final String URI      = "/";

	private static final String FORM_USERNAME           = "username";
	private static final String FORM_PASSWORD           = "pswrd";
	private static final String KEY_MESSAGE             = "%MESSAGE%";
	private static final String KEY_LOGIN_BOX           = "%LOGIN_BOX%";
	private static final String KEY_ONE_ROW_ID          = "%ID%";
	private static final String KEY_ONE_ROW_NAME        = "%NAME%";
	private static final String KEY_ONE_ROW_DESCRIPTION = "%DESCRIPTION%";

	private static final String HEADER    =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n"
					+ "<html>\n"
					+ "    <head> \n"
					+ "        <meta content=\"text/html; charset=utf-8\" http-equiv=\"content-type\"> \n"
					+ "        <title>Mikorvolt - Kezdőlap</title>         \n"
					+ "        <meta charset=\"utf-8\"> \n"
					+ "        <style> \n"
					+ "            .pubth {\n"
					+ "            color: rgb(222, 80, 37); \n"
					+ "            text-decoration: none;\n"
					+ "            }\n"
					+ "            .bigbutton {\n"
					+ "            text-decoration: none;\n"
					+ "            font-family: Agency FB;\n"
					+ "            color: white;\n"
					+ "            border-radius: 12px;\n"
					+ "            height: 40px;\n"
					+ "            width: 300px;\n"
					+ "            background-color: rgb(222, 80, 37);\n"
					+ "            border: 5px solid;\n"
					+ "            padding-top: 0px;\n"
					+ "            padding-bottom: 20px;\n"
					+ "            }\n"
					+ "            #aboutbutton {\n"
					+ "            text-decoration: none;\n"
					+ "            font-family: Agency FB;\n"
					+ "            color: white;\n"
					+ "            }\n"
					+ "            .témák {\n"
					+ "\t\t\t}\n"
					+ "\t\t</style>         \n"
					+ "    </head>\n"
					+ "    <body> \n"
					+ "        <table style=\"text-align: left; width: 100%; margin-left: auto; margin-right: auto; "
					+ "height: 331px;\" background=\"./files/history-wallpaper-9.jpg\" border=\"0\" cellpadding=\"0\" "
					+ "cellspacing=\"2\"> \n"
					+ "            <tbody> \n"
					+ "                <tr> \n"
					+ "                    <td style=\"vertical-align: middle; text-align: center;\">\n"
					+ "\t\t\t\t\t\t<big style=\"color: rgb(222, 80, 37); font-family: Agency FB;\">\n"
					+ "\t\t\t\t\t\t\t<big><big><big><big><big><big>\n"
					+ "\t\t\t\t\t\t\t\t<span style=\"font-weight: bold;\">\n"
					+ "\t\t\t\t\t\t\t\t\t<b>MIKOR VOLT?</b>\n"
					+ "\t\t\t\t\t\t\t\t</span>\n"
					+ "\t\t\t\t\t\t\t</big></big></big></big></big></big>\n"
					+ "\t\t\t\t\t\t</big>\n"
					+ "\t\t\t\t\t\t<br style=\"font-family: Agency FB;\">\n"
					+ "\t\t\t\t\t\t<br style=\"font-family: Agency FB;\">\n"
					+ "\t\t\t\t\t\t<big style=\"font-family: Agency FB;\"><big>\n"
					+ "\t\t\t\t\t\t\t<span style=\"color: white;\">TÖRTÉNELMI ÉVSZÁMOK GYAKORLÁSA</span>\n"
					+ "\t\t\t\t\t\t</big></big>\n"
					+ "\t\t\t\t\t</td> \n"
					+ "                </tr>\n"
					+ "                <tr>\n"
					+ "                    <td style=\"vertical-align: bottom; text-align: right;\">\n"
					+ "                        <a id=\"aboutbutton\" href=\"./about/\"><big><big><big>Rólunk</big"
					+ "></big></big></big></a>\n"
					+ "                    </td>\n"
					+ "                </tr>\n"
					+ "            </tbody>\n"
					+ "        </table>         \n"
					+ "        <br> \n"
					+ KEY_LOGIN_BOX
					+ "        <p style=\"color:red;text-align:center;\">" + KEY_MESSAGE + "</p>\n"
					+ "        <h1 style=\"text-decoration: underline; text-align: center;\">\n"
					+ "\t\t\t<big><big>\n"
					+ "\t\t\t\t<span style=\"font-family: Agency FB;\">TÉMAKÖRÖK</span>\n"
					+ "\t\t\t</big></big>\n"
					+ "\t\t</h1> \n"
					+ "        <br>\n"
					+ "        <table style=\"text-align: left; width: 80%\" border=\"0\" cellpadding=\"0\" "
					+ "cellspacing=\"2\" align=\"right\">\n"
					+ "\t\t\t<tbody>\n"
					+ "\t\t\t\t<tr>\n"
					+ "\t\t\t\t\t<td>\n"
					+ "\t\t\t\t\t\t<dl>\n";
	private static final String FOOTER    =
			"\t\t\t\t\t\t</dl>\n"
					+ "\t\t\t\t\t</td>\n"
					+ "\t\t\t\t</tr>\n"
					+ "\t\t\t</tbody>\n"
					+ "\t\t</table>\n"
					+ "    </body>\n"
					+ "</html>\n";
	private static final String ONE_ROW   =
			"\t\t\t\t\t\t\t<dt style=\"font-family: Agency FB;\">\n"
					+ "\t\t\t\t\t\t\t\t<a class=\"pubth\" href=\"./topic/" + KEY_ONE_ROW_ID + "/\">\n"
					+ "\t\t\t\t\t\t\t\t\t<big><big><big>\n"
					+ "\t\t\t\t\t\t\t\t\t\t<span>" + KEY_ONE_ROW_NAME + "</span>\n"
					+ "\t\t\t\t\t\t\t\t\t</big></big></big>\n"
					+ "\t\t\t\t\t\t\t\t</a>\n"
					+ "\t\t\t\t\t\t\t</dt> \n"
					+ "\t\t\t\t\t\t\t<dd style=\"font-family: Arial;\">" + KEY_ONE_ROW_DESCRIPTION + "</dd>\n";
	private static final String LOGIN_BOX =
			"        <!--<table style=\"text-align: left; width: 100%; height: 45%; table-layout: fixed;\" "
					+ "border=\"0\" cellpadding=\"2\" cellspacing=\"2\"> \n"
					+ "            <tbody> \n"
					+ "                <tr> \n"
					+ "                    <td style=\"vertical-align: top;\">-->\n"
					+ "                        <div id=\"loginContainer\" style=\"width:30%;margin:20px auto;\">\n"
					+ "                            <form acton=\"\" method=\"post\">\n"
					+ "                                <fieldset>\n"
					+ "                                    <big><big><big>\n"
					+ "                                        <span style=\"font-family: Agency FB; color: black;"
					+ "\">Felhasználónév:</span>\n"
					+ "                                    </big></big></big>\n"
					+ "                                    <br>\n"
					+ "                                    <input type=\"text\" style=\"color: rgb(140, 140, 140); "
					+ "font-family: Agency FB;\" name=\"" + FORM_USERNAME + "\"><br>\n"
					+ "                                    <big><big><big>\n"
					+ "                                        <span style=\"font-family: Agency FB; color: black;"
					+ "\">Jelszó:</span>\n"
					+ "                                    </big></big></big>\n"
					+ "                                    <br>\n"
					+ "                                    <input type=\"password\"style=\"color: rgb(140, 140, 140); "
					+ "font-family: Agency FB;\" name=\"" + FORM_PASSWORD + "\">\n"
					+ "                                    <br>\n"
					+ "                                    <button id=\"loginButton\" "
					+ "type=\"submit\">Bejelentkezek</button>\n"
					+ "                                </fieldset>\n"
					+ "                            </form>\n"
					+ "                        </div>\n"
					+ "                    <!--</td>\n"
					+ "                    <td style=\"vertical-align: top; text-align: center;\">\n"
					+ "\t\t\t\t\t\t<big><big><big>\n"
					+ "\t\t\t\t\t\t\t<span style=\"font-family: Agency FB;\">VAGY</span>\n"
					+ "\t\t\t\t\t\t</big></big></big>\n"
					+ "\t\t\t\t\t\t<br>\n"
					+ "\t\t\t\t\t\t</td> \n"
					+ "                    <td style=\"vertical-align: top;\">REGISZTR.<br> </td> \n"
					+ "                </tr>\n"
					+ "            </tbody>\n"
					+ "        </table>-->\n";
	private static final String DASHBOARD =
			"<div style=\"text-align:center;\"><a href=\"./editTopic/\">Saját témakörök szerkesztése</a></div>";


	private MainPage() {
	}

	private static String listTopics() {
		Topic[] topics = TopicDatabase.getTopics();
		String string = "";
		for (int i = 0; i < topics.length; i++) {
			string += ONE_ROW.replace(KEY_ONE_ROW_ID, topics[i].getId() + "")
					.replace(KEY_ONE_ROW_NAME, topics[i].getName())
					.replace(KEY_ONE_ROW_DESCRIPTION, topics[i].getDescription());
		}
		return string;
	}

	@Override
	public boolean matches(String uri) {
		return uri.equals(URI);
	}

	@Override
	public Response serve(IHTTPSession request, Session session) {
		if (request.getParms().get(FORM_USERNAME) != null
				&& request.getParms().get(FORM_PASSWORD) != null) { // Login request

			if (login(request, session)) { // Successful login
				return getMainPage("Sikeres bejelentkezés", true);
			} else { // Unsuccessful login
				return getMainPage("Sikertelen bejelentkezés", false);
			}

		} else {
			return getMainPage("", session.user != null);
		}
	}

	private boolean login(IHTTPSession request, Session session) {
		String username = Util.validateText(request.getParms().get(FORM_USERNAME)),
				password = Util.validateText(request.getParms().get(FORM_PASSWORD));
		User user = UserDatabase.login(username,password);
		session.user = user;
		return user != null;
	}

	private Response getMainPage(String message, boolean loggedIn) {
		String html = HEADER + listTopics() + FOOTER;
		html = html.replace(KEY_MESSAGE, message);
		if (loggedIn)
			html = html.replace(KEY_LOGIN_BOX, DASHBOARD);
		else
			html = html.replace(KEY_LOGIN_BOX, LOGIN_BOX);
		return NanoHTTPD.newFixedLengthResponse(html);
	}
}