package hu.bdgkodolas.mikorvolt.webserver.page;

import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.data.TopicDatabase;
import hu.bdgkodolas.mikorvolt.data.Question;
import hu.bdgkodolas.mikorvolt.data.Topic;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;

public class TopicPage implements Page {

	public static final Page INSTANCE = new TopicPage();

	protected static final String PREFIX  = "/topic/";
	private static final   String POSTFIX = "/";

	private static final String KEY_NAME        = "%NAME%";
	private static final String KEY_DESCRIPTION = "%DESCRIPTION%";
	private static final String KEY_QUESTIONS   = "%QUESTIONS%";

	private static final String HTML =
			"<!DOCTYPE html PUBLIC \"-//W3C//DTD HTML 4.01//EN\" \"http://www.w3.org/TR/html4/strict.dtd\">\n"
					+ "<html>\n"
					+ "\t<head>\n"
					+ "        <meta content=\"text/html; charset=utf-8\" http-equiv=\"content-type\">\n"
					+ "        <title>Mikorvolt - "+KEY_NAME+"</title>\n"
					+ "        <meta charset=\"utf-8\"> \n"
					+ "        <style type=\"text/css\"> \n"
					+ "            body {\n"
					+ "\t\t\t\tborder-style: hidden;\n"
					+ "\t\t\t\tborder-width: 64px;\n"
					+ "\t\t\t\tmargin: 64px;\n"
					+ "\t\t\t\tbackground-image: url(\"../files/history-wallpaper-9.jpg\");\n"
					+ "\t\t\t\tbackground-attachment: fixed;\n"
					+ "\t\t\t\tbackground-position: top center;\n"
					+ "            }\n"
					+ "            #qCContainer {\n"
					+ "\t\t\t\tbackground-color: white;\n"
					+ "\t\t\t\tborder-radius: 24px;\n"
					+ "\t\t\t\twidth: 80%\n"
					+ "\t\t\t\tborder-color: white;\n"
					+ "\t\t\t\tpadding: 12px;\n"
					+ "\t\t\t}\n"
					+ "            #questionContainer {\n"
					+ "\t\t\t\tbackground-color: white;\n"
					+ "            }\n"
					+ "\t\t</style>\n"
					+ "\t</head>\n"
					+ "\t<body style=\"background-image: url(../../files/history-wallpaper-9.jpg);\"> \n"
					+ "\t\t<table style=\"text-align: left; width: 100%; height: 64px;\" border=\"0\" "
					+ "cellpadding=\"0\" cellspacing=\"5\">\n"
					+ "\t\t\t<tbody>\n"
					+ "\t\t\t\t<tr>\n"
					+ "\t\t\t\t\t<td>\n"
					+ "\t\t\t\t\t\t<a href=\"../../\">\n"
					+ "\t\t\t\t\t\t\t<img style=\"border: 0px solid ; width: 48px; height: 48px;\" alt=\"\" "
					+ "title=\"Visszalépés\" src=\"../../files/back.png\">\n"
					+ "\t\t\t\t\t\t</a>\n"
					+ "\t\t\t\t\t</td>\n"
					+ "\t\t\t\t\t<td>\n"
					+ "\t\t\t\t\t\t<big style=\"color: rgb(222, 80, 37);\">\n"
					+ "\t\t\t\t\t\t\t<big><big><big><big>\n"
					+ "\t\t\t\t\t\t\t\t<span style=\"font-family: Agency FB;\">&nbsp;&nbsp;     <span "
					+ "style=\"font-weight: bold;\">" + KEY_NAME + "</span>                 </span>\n"
					+ "\t\t\t\t\t\t\t</big></big></big></big>\n"
					+ "\t\t\t\t\t\t</big>\n"
					+ "\t\t\t\t\t</td>\n"
					+ "\t\t\t\t\t<td style=\"text-align: right;\">\n"
					+ "\t\t\t\t\t\t<a href=\"./play/\">\n"
					+ "\t\t\t\t\t\t\t<img title=\"Teszt futtatása\" style=\"border: 0px solid ; width: 48px; "
					+ "height: 48px;\" alt=\"\" src=\"../../files/PLAY.png\">\n"
					+ "\t\t\t\t\t\t</a>\n"
					+ "\t\t\t\t\t</td>\n"
					+ "\t\t\t\t</tr>\n"
					+ "\t\t\t</tbody>\n"
					+ "\t\t</table>\n"
					+ "        <br> \n"
					+ "        <span style=\"color: white; font-family: Arial; margin-left: 15%;\">" + KEY_DESCRIPTION
					+ "</span>\n"
					+ "        <br> \n"
					+ "        <br>\n"
					+ "        <br> \n"
					+ "        <center>\n"
					+ "\t\t\t<div id=\"qCContainer\">\n"
					+ "\t\t\t\t<table id=\"questionContainer\" style=\"width: 80%\" border=\"0\" cellpadding=\"0\""
					+ " cellspacing=\"2\">\n"
					+ "\t\t\t\t\t<tbody>\n"
					+ KEY_QUESTIONS
					+ "\t\t\t\t\t</tbody>\n"
					+ "\t\t\t\t</table>\n"
					+ "\t\t\t</div>\n"
					+ "\t\t</center>         \n"
					+ "\t</body>\n"
					+ "</html>\n";

	private TopicPage() {
	}

	private static String listQuestions(Topic akármi) {
		Question[] tömb = akármi.getQuestions();
		String üres = "";
		for (int i = 0; i < tömb.length; i++)
			üres += "\t\t\t\t\t\t<tr>\n"
					+ "\t\t\t\t\t\t\t<td>\n"
					+ "\t\t\t\t\t\t\t\t<span style=\"font-family: Arial;\">\n"
					+ "\t\t\t\t\t\t\t\t\t" + tömb[i].getName() + "\n"
					+ "\t\t\t\t\t\t\t\t</span>\n"
					+ "\t\t\t\t\t\t\t</td>\n"
					+ "\t\t\t\t\t\t\t<td>\n"
					+ "\t\t\t\t\t\t\t\t<span style=\"font-family: Arial;\">\n"
					+ "\t\t\t\t\t\t\t\t\t" + tömb[i].getAnswer() + "\n"
					+ "\t\t\t\t\t\t\t\t</span>\n"
					+ "\t\t\t\t\t\t\t</td>\n"
					+ "\t\t\t\t\t\t</tr>\n";

		return üres;
	}

	private static Topic getTopic(IHTTPSession session) {
		return TopicDatabase.getTopic(TopicPageUtils.getTopicId(session.getUri(), PREFIX, POSTFIX));
	}

	@Override
	public boolean matches(String uri) {
		return TopicPageUtils.checkTopicId(TopicPageUtils.getTopicId(uri, PREFIX, POSTFIX));
	}

	@Override
	public Response serve(IHTTPSession request, Session session) {
		Topic topic = getTopic(request);
		return NanoHTTPD.newFixedLengthResponse(
				HTML.replace(KEY_NAME, topic.getName())
						.replace(KEY_DESCRIPTION, topic.getDescription())
						.replace(KEY_QUESTIONS, listQuestions(topic)));
	}
}
