package hu.bdgkodolas.mikorvolt.webserver.page;


import fi.iki.elonen.NanoHTTPD;
import fi.iki.elonen.NanoHTTPD.IHTTPSession;
import fi.iki.elonen.NanoHTTPD.Response;
import hu.bdgkodolas.mikorvolt.webserver.Page;
import hu.bdgkodolas.mikorvolt.webserver.Session;


public class NotFoundPage implements Page {

	public static final  Page   INSTANCE = new NotFoundPage();
	private static final String HTML     =
			"<!DOCTYPE html>\n" +
			"<html>\n" +
			"<head>\n" +
			"<title>Nincs ilyen oldal</title>\n" +
			"</head>\n" +
			"<body>\n" +
			"\n" +
			"<h1 style=\"color:red;text-align:center;padding-top: 300px;"
			+ "\"><b>Nincs ilyen oldal</b></h1>\n"
			+ "<a href=\"/\">Vissza a főoldalra</a>\n"
			+
			"\n" +
			"</body>\n" +
			"</html>";

	private NotFoundPage() {
	}

	@Override
	public boolean matches(String uri) {
		return true;
	}

	@Override
	public Response serve(IHTTPSession request, Session session) {
		return NanoHTTPD.newFixedLengthResponse(Response.Status.NOT_FOUND, NanoHTTPD.MIME_HTML, HTML);
	}
}
