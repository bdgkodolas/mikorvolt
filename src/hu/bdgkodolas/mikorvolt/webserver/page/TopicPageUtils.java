package hu.bdgkodolas.mikorvolt.webserver.page;

import hu.bdgkodolas.mikorvolt.data.TopicDatabase;

public class TopicPageUtils {

	private TopicPageUtils() {
	}


	/**
	 * Returns -1, if the uri is badly formatted
	 *
	 * @param uri
	 * @param postfix The last part of the uri (/topic/123/play/ -> postfix=/play/)
	 * @return
	 */
	public static long getTopicId(String uri, String prefix, String postfix) {
		int startIndex = prefix.length();
		int endIndex = uri.length() - postfix.length();
		if (endIndex == -1 || startIndex >= endIndex)
			return -1;
		try {
			return Long.parseLong(uri.substring(startIndex, endIndex));
		} catch (NumberFormatException e) {
			return -1;
		}
	}

	public static boolean checkTopicId(long id) {
		return TopicDatabase.getTopic(id) != null;
	}
}
