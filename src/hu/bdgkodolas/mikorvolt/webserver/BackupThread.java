package hu.bdgkodolas.mikorvolt.webserver;

import hu.bdgkodolas.mikorvolt.data.Backup;

public class BackupThread extends Thread {

	private int saveinterval;
	private boolean finished = false;
	private String backupFile;

	public BackupThread(int saveinterval) {
		setDaemon(true);
		this.saveinterval = saveinterval;
	}

	@Override
	public void run() {
		while (!finished) {
			WebServer.log("Backup to " + backupFile);
			Backup.backup(backupFile);
			try {
				Thread.sleep(saveinterval);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	void finish() {
		finished = true;
	}

	public void setBackupFile(String backupFile) {
		this.backupFile = backupFile;
	}
}
