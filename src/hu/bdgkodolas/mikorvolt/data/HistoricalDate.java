package hu.bdgkodolas.mikorvolt.data;

import java.time.*;
import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HistoricalDate {

	public static final int MODE_YEAR_MONTH_DAY = 0;
	public static final int MODE_YEAR_MONTH     = 1;
	public static final int MODE_YEAR           = 2;
	public static final int MODE_CENTURY        = 3;

	private static final Pattern PATTERN_YEAR_MONTH_DAY = Pattern.compile("^(\\d+)\\. (\\d+)\\. (\\d+)\\.$");
	private static final Pattern PATTERN_YEAR_MONTH     = Pattern.compile("^(\\d+)\\. (\\d+)\\.$");
	private static final Pattern PATTERN_YEAR           = Pattern.compile("^(\\d+)\\.$");
	private static final Pattern PATTERN_CENTURY        = Pattern.compile("^(\\d+)\\. század$");

	private int mode;
	private int century;
	private int year;
	private int month;
	private int day;

	// ..............multiple constructors.....................
	public HistoricalDate(int year, int month, int day) {
		boolean dateIsValid = true;
		try {
			LocalDate.of(year, month, day);
		} catch (DateTimeException e) {
			dateIsValid = false;
		}
		if (dateIsValid) {
			mode = MODE_YEAR_MONTH_DAY;
			this.year = year;
			this.month = month;
			this.day = day;
		} else {
			System.out.println("Hibás bemenet!");
		}
	}

	public HistoricalDate(int year, int month) {
		boolean dateIsValid = true;
		try {
			LocalDate.of(year, month, 1);
		} catch (DateTimeException e) {
			dateIsValid = false;
		}
		if (dateIsValid) {
			mode = MODE_YEAR_MONTH;
			this.year = year;
			this.month = month;
		} else {
			System.out.println("Hibás bemenet!");
		}
	}


	public HistoricalDate(int yearOrCentury, boolean b) {
		if (b == true) {
			if (yearOrCentury < (Calendar.getInstance().get(Calendar.YEAR) + 1)) {
				mode = MODE_YEAR;
				year = yearOrCentury;
			}

		} else {
			if (yearOrCentury < 22)
				mode = MODE_CENTURY;
			century = yearOrCentury;
		}
	}

	public HistoricalDate(String string) {
		Matcher matcher;

		matcher = PATTERN_YEAR_MONTH_DAY.matcher(string);
		if (matcher.find()) { // YMD
			mode = MODE_YEAR_MONTH_DAY;
			year = Integer.parseInt(matcher.group(1));
			month = Integer.parseInt(matcher.group(2));
			day = Integer.parseInt(matcher.group(3));
			return;
		}

		matcher = PATTERN_YEAR_MONTH.matcher(string);
		if (matcher.find()) { // YM
			mode = MODE_YEAR_MONTH;
			year = Integer.parseInt(matcher.group(1));
			month = Integer.parseInt(matcher.group(2));
			return;
		}

		matcher = PATTERN_YEAR.matcher(string);
		if (matcher.find()) { // Y
			mode = MODE_YEAR;
			year = Integer.parseInt(matcher.group(1));
			return;
		}

		matcher = PATTERN_CENTURY.matcher(string);
		if (matcher.find()) { // C
			mode = MODE_CENTURY;
			year = Integer.parseInt(matcher.group(1));
			return;
		}

		throw new IllegalArgumentException("Bad input");
	}


	//........................................

	public int getMode() {
		return mode;
	}

	public int getCentury() {
		return this.century;
	}

	public int getYear() {
		return this.year;
	}

	public int getMonth() {
		return this.month;
	}

	public int getDay() {
		return this.day;
	}

	@Override
	public String toString() {

		if (getMode() == MODE_YEAR_MONTH_DAY) {
			String s = (getYear() + ". " + getMonth() + ". " + getDay() + ".");
			return s;
		}
		if (getMode() == MODE_YEAR_MONTH) {
			String s = (getYear() + ". " + getMonth() + ".");
			return s;
		}
		if (getMode() == MODE_YEAR) {
			String s = (getYear() + ".");
			return s;
		}
		if (getMode() == MODE_CENTURY) {
			String s = (getCentury() + ". század");
			return s;
		}
		return "hiba";
	}
}

