package hu.bdgkodolas.mikorvolt.data;

import java.util.ArrayList;

public class Topic {

	/**
	 * Gets value in {@link TopicDatabase#addTopic(Topic)}
	 */
	protected long     id;
	protected boolean  isIdSet;
	protected String   ownerName;
	protected User     owner;
	private   String   name;
	private   String   description;
	private   String[] keywords;
	private ArrayList<Question> questions = new ArrayList<Question>();

	//.............................................
	public Topic(String name, String description, String[] keywords, User owner) {
		this.name = name;
		this.description = description;
		this.keywords = keywords;
		this.owner = owner;
	}

	protected Topic(long id, String name, String description, String[] keywords) {
		this(name, description, keywords, null);
		this.id = id;
		isIdSet = true;
	}

	public User getOwner() {
		return owner;
	}

	public long getId() {
		return id;
	}

	//.............................................
	public String getName() {
		return this.name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return this.description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String[] getKeywords() {
		return this.keywords;
	}

	public void setKeywords(String[] keywords) {
		this.keywords = keywords;
	}

	public Question[] getQuestions() {
		Question[] arrayOfQuestions = new Question[questions.size()];
		for (int i = 0; i < questions.size(); i++) {
			arrayOfQuestions[i] = questions.get(i);
		}
		return arrayOfQuestions;
	}

	public void addQuestion(Question question) {
		questions.add(question);
	}

	public void clear() {
		questions.clear();
	}
}


