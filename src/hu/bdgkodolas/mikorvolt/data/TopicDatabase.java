package hu.bdgkodolas.mikorvolt.data;

import java.util.HashMap;
import java.util.Map;

public class TopicDatabase {

	private static final Map<Long, Topic> topics = new HashMap<>();

	public static Topic[] getTopics() {
		synchronized (topics) {
			return topics.values().toArray(new Topic[topics.size()]);
		}
	}

	public static Topic getTopic(long id) {
		synchronized (topics) {
			return topics.get(id);
		}
	}

	public static synchronized void addTopic(Topic topic) throws Exception {
		synchronized (topics) {
			if (topic.isIdSet) {
				if (topics.get(topic.getId()) == null)
					topics.put(topic.id, topic);
				else
					throw new Exception("Wrong topic ID");
			} else {
				long id = DatabaseUtils.generateId(topics);
				synchronized (topic) {
					topic.id = id;
					topic.isIdSet = true;
				}
				topics.put(id, topic);
			}
		}
	}


	public static synchronized Topic deleteTopic(long id) {
		return topics.remove(id);
	}
}

