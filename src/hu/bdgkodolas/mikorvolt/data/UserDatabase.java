package hu.bdgkodolas.mikorvolt.data;

import java.util.HashMap;
import java.util.Map;


public class UserDatabase {
	private static final Map<String, User> users = new HashMap<>();

	public static User login(String name, String password) {
		User user = users.get(name);
		if (user != null && user.checkPassword(password))
			return user;
		return null;
	}

	public static boolean changePassword(String username, String oldPassword, String newPassword) {
		User user = users.get(username);
		if (user == null)
			return false;
		if (user.checkPassword(oldPassword)) {
			user.updatePassword(newPassword, false);
			return true;
		} else {
			return false;
		}
	}


	public static User[] getUsers() { // TODO +
		User[] arrayOfUsers = new User[users.size()];
		int i = 0;
		for (User user : users.values()) {
			arrayOfUsers[i] = user;
			i++;
		}
		return arrayOfUsers;
	}

	public static void addUser(User user) { // TODO +
		users.put(user.getName(), user);
	}

	public static void register(String newName, String newPass) {
		// TODO check if the name is already in use +
		boolean csekk = true;
		for (User user : users.values()) {
			if (user.getName().equals(newName)) {
				csekk = false;
				break;
			}
		}
		if (csekk)
			users.put(newName, new User(newName, newPass, false));
	}

	protected static User getUser(String name) {
		return users.get(name);
	}
}